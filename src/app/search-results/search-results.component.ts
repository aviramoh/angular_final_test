import { ProductsService } from './../products/products.service';
import { ActivatedRoute } from '@angular/router';
import { ProductsComponent } from './../products/products.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {
  product;
  service:ProductsService;

  constructor(private route: ActivatedRoute, service: ProductsService) { 
    this.service = service;
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
     let id = params.get('id');
      console.log(id);
      this.service.searchProduct(id).subscribe(response=>{
        this.product = response.json();
        console.log(this.product);
      })
    })
  }

}
