import { ProductsService } from './../products.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  @Output() addProduct:EventEmitter<any> = new EventEmitter<any>(); //any is the type. could be [] or {}
  @Output() addProductPs:EventEmitter<any> = new EventEmitter<any>(); //pessimistic

  service:ProductsService;
  
  pdtform = new FormGroup({
    name:new FormControl(),
    price:new FormControl(),
  });

  constructor(private route: ActivatedRoute ,service: ProductsService, private router:Router) {
    this.service = service;
   }

   sendData() {
    //הפליטה של העדכון לאב
  this.addProduct.emit(this.pdtform.value.name);
  console.log(this.pdtform.value);
  this.route.paramMap.subscribe(params=>{
    let id = params.get('id');
    this.service.putProduct(this.pdtform.value, id).subscribe(
      response => {
        console.log(response.json());
        this.addProductPs.emit();
        this.router.navigateByUrl("/");//return to main page
      }
    );
  })
}

  product;

  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
     let id = params.get('id');
      console.log(id);
      this.service.getProduct(id).subscribe(response=>{
        this.product = response.json();
        console.log(this.product);
      })
    })
  }

}
