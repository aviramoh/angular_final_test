import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class ProductsService {

  http:Http;

  getProducts() {
    return this.http.get(environment.url +'products');
  }

  getProduct(id) {
    return this.http.get(environment.url+'products/' + id);
  }

  putProduct(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.put(environment.url+'products/'+ key,params.toString(), options);
  }

  searchProduct(name) {
    return this.http.get(environment.url+'search/' + name);
  }

  constructor(http:Http) {
    this.http = http
   }

}
