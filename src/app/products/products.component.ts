import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { ProductsService } from './products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  product;
  showResults = false;
  products;
  productsKeys = [];

  searchForm = new FormGroup({
    search:new FormControl(),
  });
  
  constructor(private service:ProductsService, private route: ActivatedRoute, private router:Router) {
     service.getProducts().subscribe(response => {
      this.products = response.json();
      this.productsKeys = Object.keys(this.products);
      });
   }

   sendData() {
  this.route.paramMap.subscribe(params=>{
    this.service.searchProduct(this.searchForm.value.search).subscribe(
      response => {
        this.product = response.json();
        this.showResults = true;
        console.log(this.product);
        this.router.navigateByUrl("/");//return to main page
      }
    );
  })
}

  ngOnInit() {
  }

}
