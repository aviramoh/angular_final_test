import { FirebaseService } from './firebase.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-firebase',
  templateUrl: './firebase.component.html',
  styleUrls: ['./firebase.component.css']
})
export class FirebaseComponent implements OnInit {

  constructor(private service:FirebaseService) { }

  products;

  ngOnInit() {
    this.service.getProductsFire().subscribe(response=>{
      console.log(response);
      this.products = response;
    })
  }

}
