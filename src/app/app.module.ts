import { EditProductComponent } from './products/edit-product/edit-product.component';
import { ProductsService } from './products/products.service';
import { FirebaseService } from './firebase/firebase.service';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { FirebaseComponent } from './firebase/firebase.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { HttpModule } from '@angular/http';
import { SearchResultsComponent } from './search-results/search-results.component';


@NgModule({
  declarations: [
    AppComponent,
    FirebaseComponent,
    NavigationComponent,
    NotFoundComponent,
    ProductsComponent,
    LoginComponent,
    EditProductComponent,
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'', component:ProductsComponent},
      {path:'firebase', component:FirebaseComponent},
      {path:'edit-product/:id', component:EditProductComponent},
      {path:'search-results', component:SearchResultsComponent},
      {path:'**', component:NotFoundComponent}//have to be last
    ])
  ],
  providers: [
    FirebaseService,
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
