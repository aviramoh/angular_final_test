// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase: {
    apiKey: "AIzaSyCNva0plamSw2291W__BuD5r17tzckivJA",
    authDomain: "angulartest-32dd7.firebaseapp.com",
    databaseURL: "https://angulartest-32dd7.firebaseio.com",
    projectId: "angulartest-32dd7",
    storageBucket: "angulartest-32dd7.appspot.com",
    messagingSenderId: "920760053403"
  } 
};
